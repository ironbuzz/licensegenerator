﻿using System;
using System.Threading;
using System.Windows.Forms;
using log4net;
using log4net.Config;
using System.Net.NetworkInformation;
using System.Linq;

namespace LicenseGenerator
{
    public partial class LicenseGenerator : Form
    {
        private readonly ILog log = LogManager.GetLogger(typeof(LicenseGenerator));
        public LicenseGenerator()
        {
            InitializeComponent();
            gbExpiration.Visible = !cbPermanent.Checked;
        }

        private void btOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(tbCustomer.Text))
                {
                    errorProvider1.SetError(tbCustomer, "This field is mandatory");
                    return;
                }
                if (!cbPermanent.Checked && dtExpirationDate.Value <= DateTime.Now.AddDays(30))
                {
                    errorProvider1.SetError(dtExpirationDate, "Please set expripation date more that 30 days");
                    return;
                }
                DialogResult = DialogResult.OK;
                //var dlg = new SaveFileDialog()
                //{
                //    Filter = @"LicenseFile|*irb",
                //    Title = "Save a license File"
                //};
                string selectedPath = null;
                var t = new Thread(() =>
                {
                    if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                        return;

                    selectedPath = saveFileDialog1.FileName;
                });

                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                t.Join();
                if (!string.IsNullOrEmpty(selectedPath))
                {
                    log.InfoFormat("Generating license for {0}, MAC address: {1}, {2} {3}, {4}", tbCustomer.Text, tbMac.Text, "license",
                        cbPermanent.Checked ? "is permanent": "expiration date is: " + dtExpirationDate.Text, 
                        string.IsNullOrEmpty(tbItemNumber.Text)? "":" number of items allowed: " + tbItemNumber.Text);
                    int itemNumber = int.Parse(tbItemNumber.Text);
                    LicenseManager.GetInstance(selectedPath).CreateLicense(selectedPath, tbCustomer.Text, tbMac.Text, cbPermanent.Checked, dtExpirationDate.Value, itemNumber);
                    Close();
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error during license generation", ex);
            }
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = DialogResult.None;
                var t = new Thread(() =>
                {
                    res = MessageBox.Show("License Generator", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                });
                t.SetApartmentState(ApartmentState.STA);
                t.Start();
                t.Join();
                if (res == DialogResult.No)
                    return;
                DialogResult = DialogResult.Cancel;
                Close();
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error during cancelling license generation", ex);
            }
        }

        private void cbPermanent_CheckedChanged(object sender, EventArgs e)
        {
            gbExpiration.Visible = !cbPermanent.Checked;
        }

        private void LicenseGenerator_Load(object sender, EventArgs e)
        {
            BasicConfigurator.Configure();
            this.tbItemNumber.Maximum = decimal.MaxValue;
        }

        private void currentMacButton_Click(object sender, EventArgs e)
        {
            try
            {
                tbMac.Text = LicenseManager.GetLocalMachineMacAddress();
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error during MAC address retrieval", ex);
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
