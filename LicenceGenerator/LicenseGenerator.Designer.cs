﻿namespace LicenseGenerator
{
    partial class LicenseGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btOk = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbCustomer = new System.Windows.Forms.TextBox();
            this.cbPermanent = new System.Windows.Forms.CheckBox();
            this.gbExpiration = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtExpirationDate = new System.Windows.Forms.DateTimePicker();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.tbMac = new System.Windows.Forms.TextBox();
            this.currentMacButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tbItemNumber = new System.Windows.Forms.NumericUpDown();
            this.gbExpiration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbItemNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // btOk
            // 
            this.btOk.Location = new System.Drawing.Point(110, 212);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(75, 23);
            this.btOk.TabIndex = 5;
            this.btOk.Text = "OK";
            this.btOk.UseVisualStyleBackColor = true;
            this.btOk.Click += new System.EventHandler(this.btOk_Click);
            // 
            // btCancel
            // 
            this.btCancel.Location = new System.Drawing.Point(191, 212);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 6;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Customer Name:";
            // 
            // tbCustomer
            // 
            this.tbCustomer.Location = new System.Drawing.Point(144, 24);
            this.tbCustomer.Name = "tbCustomer";
            this.tbCustomer.Size = new System.Drawing.Size(180, 20);
            this.tbCustomer.TabIndex = 0;
            // 
            // cbPermanent
            // 
            this.cbPermanent.AutoSize = true;
            this.cbPermanent.Location = new System.Drawing.Point(27, 118);
            this.cbPermanent.Name = "cbPermanent";
            this.cbPermanent.Size = new System.Drawing.Size(91, 17);
            this.cbPermanent.TabIndex = 3;
            this.cbPermanent.Text = "Is Permanent:";
            this.cbPermanent.UseVisualStyleBackColor = true;
            this.cbPermanent.CheckedChanged += new System.EventHandler(this.cbPermanent_CheckedChanged);
            // 
            // gbExpiration
            // 
            this.gbExpiration.Controls.Add(this.label2);
            this.gbExpiration.Controls.Add(this.dtExpirationDate);
            this.gbExpiration.Location = new System.Drawing.Point(124, 118);
            this.gbExpiration.Name = "gbExpiration";
            this.gbExpiration.Size = new System.Drawing.Size(200, 76);
            this.gbExpiration.TabIndex = 5;
            this.gbExpiration.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Valid until:";
            // 
            // dtExpirationDate
            // 
            this.dtExpirationDate.CustomFormat = "MM/dd/yyyy HH:mm:ss";
            this.dtExpirationDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtExpirationDate.Location = new System.Drawing.Point(66, 34);
            this.dtExpirationDate.Name = "dtExpirationDate";
            this.dtExpirationDate.Size = new System.Drawing.Size(129, 20);
            this.dtExpirationDate.TabIndex = 4;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "license.irb";
            this.saveFileDialog1.Filter = "LicenseFile|*.irb";
            this.saveFileDialog1.Title = "Save a license File";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 56);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Customer machine MAC :";
            // 
            // tbMac
            // 
            this.tbMac.Location = new System.Drawing.Point(145, 54);
            this.tbMac.Name = "tbMac";
            this.tbMac.Size = new System.Drawing.Size(180, 20);
            this.tbMac.TabIndex = 1;
            // 
            // currentMacButton
            // 
            this.currentMacButton.Location = new System.Drawing.Point(331, 51);
            this.currentMacButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.currentMacButton.Name = "currentMacButton";
            this.currentMacButton.Size = new System.Drawing.Size(71, 23);
            this.currentMacButton.TabIndex = 2;
            this.currentMacButton.Text = "get current";
            this.currentMacButton.UseVisualStyleBackColor = true;
            this.currentMacButton.Click += new System.EventHandler(this.currentMacButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Number of items allowed:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // tbItemNumber
            // 
            this.tbItemNumber.Location = new System.Drawing.Point(145, 83);
            this.tbItemNumber.Name = "tbItemNumber";
            this.tbItemNumber.Size = new System.Drawing.Size(174, 20);
            this.tbItemNumber.TabIndex = 8;
            // 
            // LicenseGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(411, 249);
            this.Controls.Add(this.tbItemNumber);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.currentMacButton);
            this.Controls.Add(this.tbMac);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.gbExpiration);
            this.Controls.Add(this.cbPermanent);
            this.Controls.Add(this.tbCustomer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btOk);
            this.Name = "LicenseGenerator";
            this.Text = "LicenseGenerator";
            this.Load += new System.EventHandler(this.LicenseGenerator_Load);
            this.gbExpiration.ResumeLayout(false);
            this.gbExpiration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbItemNumber)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbCustomer;
        private System.Windows.Forms.CheckBox cbPermanent;
        private System.Windows.Forms.GroupBox gbExpiration;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtExpirationDate;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button currentMacButton;
        private System.Windows.Forms.TextBox tbMac;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown tbItemNumber;
    }
}